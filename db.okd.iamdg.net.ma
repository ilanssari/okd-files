$TTL    604800
@       IN      SOA     services.okd.iamdg.net.ma. admin.okd.iamdg.net.ma. (
         2021052701     ; Serial
             604800     ; Refresh
              86400     ; Retry
            2419200     ; Expire
             604800     ; Negative Cache TTL
)

; name servers - NS records
    IN      NS      services

; name servers - A records
services.okd.iamdg.net.ma.          IN      A       10.96.10.210

; OKD Cluster - A records
bootstrap.cluster.okd.iamdg.net.ma.        IN      A      10.96.10.200
control-plane-1.cluster.okd.iamdg.net.ma.        IN      A      10.96.10.201
control-plane-2.cluster.okd.iamdg.net.ma.         IN      A      10.96.10.202
control-plane-3.cluster.okd.iamdg.net.ma.         IN      A      10.96.10.203
compute-1.cluster.okd.iamdg.net.ma.        IN      A      10.96.10.204
compute-2.cluster.okd.iamdg.net.ma.        IN      A      10.96.10.205
loadbalancer-1.cluster.okd.iamdg.net.ma.        IN      A      10.96.10.206

; OKD internal cluster IPs - A records
api.cluster.okd.iamdg.net.ma.    IN    A    10.96.10.206
api-int.cluster.okd.iamdg.net.ma.    IN    A    10.96.10.206
*.apps.cluster.okd.iamdg.net.ma.    IN    A    10.96.10.206
etcd-0.cluster.okd.iamdg.net.ma.    IN    A     10.96.10.201
etcd-1.cluster.okd.iamdg.net.ma.    IN    A     10.96.10.202
etcd-2.cluster.okd.iamdg.net.ma.    IN    A    10.96.10.203
console-openshift-console.apps.cluster.okd.iamdg.net.ma.     IN     A     10.96.10.206
oauth-openshift.apps.cluster.okd.iamdg.net.ma.     IN     A     10.96.10.206

; OKD internal cluster IPs - SRV records
_etcd-server-ssl._tcp.cluster.okd.iamdg.net.ma.    86400     IN    SRV     0    10    2380    etcd-0.cluster
_etcd-server-ssl._tcp.cluster.okd.iamdg.net.ma.    86400     IN    SRV     0    10    2380    etcd-1.cluster
_etcd-server-ssl._tcp.cluster.okd.iamdg.net.ma.    86400     IN    SRV     0    10    2380    etcd-2.cluster

